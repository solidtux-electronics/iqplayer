# IQPlayer

## Installation

### AppImage

[Download](https://gitlab.com/solidtux-electronics/iqplayer/builds/artifacts/master/raw/IQPlayer-x86_64.AppImage?job=deploy:linux)

### From Source

* [Install Rust](https://rustup.rs)
* Install GTK3 (development version, version >=3.10)
* run `cargo install --path .`
