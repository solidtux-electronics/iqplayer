use byteorder::{LittleEndian, ReadBytesExt};
use failure::Error;
use rustfft::num_complex::Complex;

use std::{
    fs::{metadata, File},
    io::{Read, Seek, SeekFrom, Write},
    path::Path,
};

pub struct IqRecording {
    file: File,
    size: u64,
}

impl IqRecording {
    pub fn new<T: AsRef<Path>>(path: T) -> Result<IqRecording, Error> {
        let metadata = metadata(&path)?;
        Ok(IqRecording {
            file: File::open(path)?,
            size: metadata.len() / 8,
        })
    }

    pub fn get_size(&self) -> u64 {
        return self.size;
    }

    pub fn get_data(&mut self, start: u64, range: u64) -> Result<Vec<Complex<f32>>, Error> {
        self.file.seek(SeekFrom::Start(start * 8))?;
        let mut res = Vec::new();
        for _ in 0..range {
            let real = self.file.read_f32::<LittleEndian>()?;
            let imag = self.file.read_f32::<LittleEndian>()?;
            res.push(Complex::new(real, imag));
        }
        Ok(res)
    }

    pub fn save_data<T: AsRef<Path>>(
        &mut self,
        start: u64,
        range: u64,
        filename: T,
    ) -> Result<(), Error> {
        self.file.seek(SeekFrom::Start(start * 8))?;
        let mut file = File::create(filename)?;
        let mut buf = [0; 8];
        for _ in 0..range {
            self.file.read_exact(&mut buf)?;
            let _ = file.write(&buf)?;
        }
        Ok(())
    }
}
