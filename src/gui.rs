use super::{colormap::VIRIDIS, iq::IqRecording};
use gdk_pixbuf::{Colorspace, Pixbuf};
use gtk::{
    self, prelude::*, Adjustment, Box, Button, FileChooserAction, FileChooserDialog, Image,
    Inhibit, Label, Orientation, ResponseType, SpinButton, Window, WindowPosition, WindowType,
};
use relm::{Relm, Update, Widget};
use rustfft::{num_complex::Complex, num_traits::Zero, FftDirection, FftPlanner};

const WIDTH: i32 = 1024;
const HEIGHT: i32 = 512;

pub struct Model {
    recording: IqRecording,
    filename: String,
}

#[derive(Msg)]
pub enum Msg {
    Info,
    Step,
    Save,
    Update,
    Quit,
}

pub struct Win {
    image: Image,
    start: SpinButton,
    range: SpinButton,
    position: Label,
    res: SpinButton,
    model: Model,
    window: Window,
}

impl Win {
    fn update_fft(&mut self) {
        self.update_info();
        let buf = Pixbuf::new(Colorspace::Rgb, false, 8, WIDTH, HEIGHT).unwrap();
        let start = self.start.value_as_int();
        let range = self.range.value_as_int();
        let res = self.res.value_as_int();
        let mut planner = FftPlanner::new();
        let fft = planner.plan_fft(WIDTH as usize, FftDirection::Forward);
        let mut amp = Vec::new();
        let mut min_val = ::std::f32::INFINITY;
        let mut max_val = ::std::f32::NEG_INFINITY;
        let mut b = vec![0.; WIDTH as usize];
        for i in 0..HEIGHT {
            let ind = (i as f64) * (range as f64) / (HEIGHT as f64);
            let mut data = self
                .model
                .recording
                .get_data((start as u64) + (ind as u64), WIDTH as u64)
                .unwrap_or(vec![Complex::zero(); WIDTH as usize]);
            if i % res == 0 {
                fft.process(&mut data);
                let mut a = data
                    .iter()
                    .map(|x| {
                        let y = x.norm().log(10.);
                        if y.is_normal() {
                            y
                        } else {
                            0.
                        }
                    })
                    .collect::<Vec<_>>();
                b = a[(WIDTH as usize / 2)..a.len()].to_vec();
                b.append(&mut a[0..(WIDTH as usize / 2)].to_vec());
            }
            for x in &b {
                if x.is_normal() {
                    min_val = min_val.min(*x);
                    max_val = max_val.max(*x);
                }
            }
            amp.push(b.clone());
        }
        for i in 0..HEIGHT {
            let row = &amp[i as usize];
            for j in 0..WIDTH {
                let x = (row[j as usize] - min_val) / (max_val - min_val);
                let ind = (255. * x) as usize;
                let (r, g, b) = VIRIDIS[ind];
                let r = (255. * r) as u8;
                let g = (255. * g) as u8;
                let b = (255. * b) as u8;
                buf.put_pixel(j as u32, (HEIGHT - i - 1) as u32, r, g, b, 255);
            }
        }
        self.image.set_from_pixbuf(Some(&buf));
    }

    fn update_info(&mut self) {
        let start = self.start.value();
        let size = self.model.recording.get_size() as f64;
        self.position
            .set_text(&format!("Position: {:.2}%", 100. * start / size));
    }
}

impl Update for Win {
    type Model = Model;
    type ModelParam = String;
    type Msg = Msg;

    fn model(_: &Relm<Self>, file: String) -> Model {
        Model {
            recording: IqRecording::new(&file).unwrap(),
            filename: file,
        }
    }

    fn update(&mut self, event: Msg) {
        match event {
            Msg::Update => self.update_fft(),
            Msg::Quit => gtk::main_quit(),
            Msg::Save => {
                self.update_info();
                let dialog = FileChooserDialog::with_buttons(
                    Some("Save IQ File"),
                    Some(&self.window),
                    FileChooserAction::Save,
                    &[
                        ("_Cancel", ResponseType::Cancel),
                        ("_Open", ResponseType::Accept),
                    ],
                );
                if dialog.run() == ResponseType::Accept.into() {
                    if let Some(filename) = dialog.filename() {
                        let start = self.start.value_as_int();
                        let range = self.range.value_as_int();
                        self.model
                            .recording
                            .save_data(start as u64, range as u64, filename)
                            .unwrap();
                    }
                }
                unsafe {
                    dialog.destroy();
                }
            }
            Msg::Step => {
                self.start
                    .set_value(self.start.value() + self.range.value());
                self.update_info();
            }
            Msg::Info => self.update_info(),
        }
    }
}

impl Widget for Win {
    type Root = Window;

    fn root(&self) -> Self::Root {
        self.window.clone()
    }

    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        let window = Window::new(WindowType::Toplevel);
        window.set_resizable(false);
        window.set_position(WindowPosition::Center);
        window.set_title(&format!("IQPlayer: {}", model.filename));

        let vbox = Box::new(Orientation::Vertical, 0);
        let image = Image::new();
        let hbox = Box::new(Orientation::Horizontal, 0);
        let bbox = Box::new(Orientation::Horizontal, 0);
        let ibox = Box::new(Orientation::Horizontal, 0);
        let start_label = Label::new(Some("Start"));
        let start = SpinButton::new(
            Some(&Adjustment::new(
                0.,
                0.,
                model.recording.get_size() as f64,
                10.,
                100.,
                0.0,
            )),
            10.,
            0,
        );
        let range_label = Label::new(Some("Range"));
        let range = SpinButton::new(
            Some(&Adjustment::new(
                HEIGHT as f64,
                0.,
                model.recording.get_size() as f64,
                10.,
                100.,
                0.0,
            )),
            10.,
            0,
        );
        let res_label = Label::new(Some("Resolution"));
        let res = SpinButton::new(
            Some(&Adjustment::new(
                HEIGHT as f64,
                1.,
                HEIGHT as f64,
                1.,
                10.,
                0.0,
            )),
            1.,
            0,
        );
        let step = Button::with_label("Step");
        let update = Button::with_label("Update");
        let save = Button::with_label("Save");
        let size = Label::new(Some(
            format!("Total Samples: {}", model.recording.get_size()).as_str(),
        ));
        let position = Label::new(None);

        hbox.pack_start(&start_label, true, true, 10);
        hbox.pack_start(&start, true, true, 10);
        hbox.pack_start(&range_label, true, true, 10);
        hbox.pack_start(&range, true, true, 10);
        hbox.pack_start(&res_label, true, true, 10);
        hbox.pack_start(&res, true, true, 10);
        bbox.pack_start(&step, true, true, 10);
        bbox.pack_start(&update, true, true, 10);
        bbox.pack_start(&save, true, true, 10);
        ibox.pack_start(&size, true, true, 10);
        ibox.pack_start(&position, true, true, 10);
        vbox.pack_start(&image, true, true, 10);
        vbox.pack_start(&hbox, true, true, 10);
        vbox.pack_start(&bbox, true, true, 10);
        vbox.pack_start(&ibox, true, true, 10);

        window.add(&vbox);

        connect!(
            relm,
            window,
            connect_delete_event(_, _),
            return (Some(Msg::Quit), Inhibit(false))
        );
        connect!(relm, start, connect_changed(_), Msg::Info);
        connect!(relm, range, connect_changed(_), Msg::Info);
        connect!(relm, step, connect_clicked(_), Msg::Step);
        connect!(relm, update, connect_clicked(_), Msg::Update);
        connect!(relm, save, connect_clicked(_), Msg::Save);
        window.show_all();

        let mut res = Win {
            image,
            start,
            range,
            model,
            res,
            position,
            window,
        };
        res.update_info();
        res.update_fft();
        res
    }
}
