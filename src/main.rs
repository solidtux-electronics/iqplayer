extern crate gtk;
#[macro_use]
extern crate relm;
#[macro_use]
extern crate relm_derive;
extern crate byteorder;
extern crate failure;
extern crate gdk_pixbuf;
extern crate rustfft;

use relm::Widget;

use std::env;

mod colormap;
mod gui;
mod iq;

use gui::Win;

fn main() {
    let filename = env::args().nth(1).unwrap();
    Win::run(filename).unwrap();
}
