#!/bin/bash

set -e

NAME=IQPlayer
BINARY=iqplayer

cargo build --release

mkdir -p build
cd build

if [ ! -f appimagetool-x86_64.AppImage ]; then
    wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage
    chmod +x appimagetool-x86_64.AppImage
fi

if [ -d $NAME.AppDir ]; then
    rm -r $NAME.AppDir
fi

mkdir -p $NAME.AppDir/libs
cp ../target/release/$BINARY $NAME.AppDir/$BINARY
cp ../files/* $NAME.AppDir
cp `ldd ../target/release/$BINARY | grep '=>' | sed -e 's/.*=> \([^ ]*\).*/\1/' | tr '\n' ' '` $NAME.AppDir/libs
cat > $NAME.AppDir/AppRun << EOF
#!/bin/bash

HERE="\$(dirname "\$(readlink -f "\${0}")")"
export LD_LIBRARY_PATH=./libs
exec "\${HERE}/$BINARY" "\$@"
EOF
chmod a+x $NAME.AppDir/AppRun

./appimagetool-x86_64.AppImage --appimage-extract
./squashfs-root/AppRun $NAME.AppDir
